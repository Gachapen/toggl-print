extern crate chrono;
extern crate clap;
extern crate csv;
extern crate reqwest;
extern crate serde;
#[macro_use]
extern crate serde_derive;

use std::process::exit;
use std::fmt::{self, Display, Formatter};
use std::path::Path;
use chrono::{Duration, NaiveDate, NaiveTime, Utc};
use clap::{App, Arg, ArgMatches, SubCommand};
use reqwest::Client;
use reqwest::header::{Authorization, Basic, Headers};

fn main() {
    let matches = App::new("Toggl to Tripletex converter")
        .version("0.1.0")
        .author("Magnus Bjerke Vik <mbvett@gmail.com>")
        .about("Converts exported detailed reports from Toggle to a nice format to use in Tripletex comments")
        .subcommand(SubCommand::with_name("from-file")
            .about("Converts exported CSV detailed report")
            .arg(Arg::with_name("INPUT")
                .help("Input csv file to convert")
                .required(true)
                .index(1)
            )
        )
        .subcommand(SubCommand::with_name("from-api")
            .about("Converts detailed report from API")
            .arg(Arg::with_name("api-token")
                .long("api-token")
                .short("t")
                .help("API token")
                .takes_value(true)
                .required(true)
            )
            .arg(Arg::with_name("user")
                .long("user")
                .short("u")
                .help("Name of user-agent or email to get data from")
                .takes_value(true)
                .required(true)
            )
            .arg(Arg::with_name("workspace")
                .long("workspace")
                .short("w")
                .help("ID of workspace to get data from")
                .takes_value(true)
                .required(true)
            )
            .arg(Arg::with_name("date")
                .long("date")
                .short("d")
                .help("ISO 8601 date to get report from")
                .takes_value(true)
            )
        )
        .get_matches();

    match matches.subcommand() {
        ("from-file", Some(matches)) => parse_from_file(matches.value_of("INPUT").unwrap()),
        ("from-api", Some(matches)) => parse_from_api(matches),
        (name, _) => println!("Unrecognized subcommand '{}'", name),
    }
}

fn parse_from_file<P: AsRef<Path>>(file_path: P) {
    let reader = csv::ReaderBuilder::new()
        .has_headers(true)
        .from_path(file_path);
    let mut reader = match reader {
        Ok(reader) => reader,
        Err(error) => {
            println!("Failed to open input file: {}", error);
            exit(1);
        }
    };

    let records: Result<Vec<TogglDetailedReportEntry>, csv::Error> = reader.deserialize().collect();
    let records = match records {
        Ok(records) => records,
        Err(error) => {
            println!("Failed to parse some csv lines: {}", error);
            exit(1);
        }
    };

    for record in records {
        println!("{}", record);
    }

    #[derive(Deserialize, Debug)]
    #[serde(rename_all = "PascalCase")]
    struct TogglDetailedReportEntry {
        user: String,
        email: String,
        project: String,
        task: String,
        description: String,
        billable: String,
        #[serde(rename = "Start date")]
        start_date: NaiveDate,
        #[serde(rename = "Start time")]
        start_time: NaiveTime,
        #[serde(rename = "End date")]
        end_date: NaiveDate,
        #[serde(rename = "End time")]
        end_time: NaiveTime,
        duration: String,
        tags: String,
    }

    impl Display for TogglDetailedReportEntry {
        fn fmt(&self, f: &mut Formatter) -> fmt::Result {
            write!(
                f,
                "{} ({}): {}",
                self.duration, self.project, self.description
            )
        }
    }
}

fn parse_from_api(matches: &ArgMatches) {
    let api_token = matches.value_of("api-token").unwrap();
    let user_agent = matches.value_of("user").unwrap();
    let workspace_id = matches.value_of("workspace").unwrap();
    let date = matches
        .value_of("date")
        .map(str::to_string)
        .unwrap_or_else(|| Utc::now().date().to_string());

    let mut headers = Headers::new();
    headers.set(Authorization(Basic {
        username: api_token.to_string(),
        password: Some("api_token".to_string()),
    }));
    let client = Client::builder().default_headers(headers).build();
    let client = match client {
        Ok(client) => client,
        Err(error) => {
            println!("Failed to build HTTP client: {}", error);
            exit(1);
        }
    };

    let url = format!("https://toggl.com/reports/api/v2/details?workspace_id={workspace}&since={date}&until={date}&user_agent={agent}",
                workspace = workspace_id,
                date = date,
                agent = user_agent
            );

    let mut response = match client.get(&url).send() {
        Ok(response) => response,
        Err(error) => {
            println!("Failed to get toggle entries from API: {}", error);
            exit(1);
        }
    };

    if !response.status().is_success() {
        println!(
            "Failed to retrieve toggle entries: Status code {}",
            response.status()
        );
        exit(1);
    }

    let report: Report = response.json().expect("Failed parsing response content");
    println!(
        "Total time: {:.1}",
        report.total_grand as f32 / 1000.0 / 60.0 / 60.0
    );
    let mut entries = report.data;
    entries.sort_unstable_by(|a, b| a.project.cmp(&b.project));
    for entry in entries {
        println!("{}", entry);
    }

    #[derive(Deserialize)]
    struct Report {
        total_grand: u32,
        data: Vec<Entry>,
    }

    #[derive(Deserialize)]
    struct Entry {
        description: String,
        dur: u32,
        project: String,
    }

    impl Display for Entry {
        fn fmt(&self, f: &mut Formatter) -> fmt::Result {
            write!(
                f,
                "{} ({}): {}",
                pretty_duration(&Duration::milliseconds(self.dur as i64)),
                self.project,
                self.description
            )
        }
    }
}

fn pretty_duration(duration: &Duration) -> String {
    format!(
        "{hh:02}:{mm:02}",
        hh = duration.num_hours(),
        mm = duration.num_minutes() - duration.num_hours() * 60
    )
}
